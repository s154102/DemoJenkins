import com.company.Calculator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class CalculatorTest {

    @Test
    public  void returnZeroIfEmpty() throws Exception {
        Assertions.assertEquals(0, Calculator.Add(""));
    }
    @Test
    public  void returnOneNumber()throws Exception {
        Assertions.assertEquals(1, Calculator.Add("1"));
    }
    @Test
    public  void returnTwoNumber() throws Exception{
        Assertions.assertEquals(3, Calculator.Add("1,2"));
    }
    @Test
    public  void returnThreeNumbersWithLineBreak()throws Exception {
        Assertions.assertEquals(6, Calculator.Add("1\n2,3"));
    }
    @Test
    public  void customeDelimiters()throws Exception {
        Assertions.assertEquals(9, Calculator.Add("//;\n1;2;3;3"));
    }
    /*
    @Test
    public  void CheckForNegatives()throws Exception {
        Assertions.assertEquals(9, Calculator.Add("//;\n1;2;3;-3"));
    }
    */
     }
