package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Calculator {

    public Calculator() {
    }

    public static int Add(String text) throws Exception {
        if (CheckforNegativNumbers(text)){
            throw new Exception("Negatives not Allowed");
        }
        String costumeDelimiter = "[\r\n,]";

        if (text.isEmpty()) {
            return 0;
        } if (text.startsWith("//")) {
            costumeDelimiter = "[\r\n" + CheckForCostumeDelimiters(text) + "]";
        }
            String[] textSeperated = text.split(costumeDelimiter, 0);
            int sum = 0;

        if (text.startsWith("//")) {
            for (int i = 2; i < textSeperated.length; i++) {
                sum += (Integer.parseInt(textSeperated[i]));
            }
        }
        else {
            for (int i = 0; i < textSeperated.length; i++) {
                sum += (Integer.parseInt(textSeperated[i]));
            }
        }
        return  sum;
    }

    public static String CheckForCostumeDelimiters (String string){

        String delimiter = Character.toString(string.charAt(2));

        return delimiter;
    }

    public static boolean CheckforNegativNumbers(String text) {

        if(text.contains("-")){
            return true;
        }
        else {
            return false;
        }
    }
}
